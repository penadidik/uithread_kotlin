package di2k.csi.uithread
//Created by 디딬 Didik M. Hadiningrat on 15 July 2019
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Thread(Runnable {
            var i = 0
            while (i < Int.MAX_VALUE){
                i++
            }

            this@MainActivity.runOnUiThread(java.lang.Runnable(){
                this.textview_msg.text = "Update text"
            })
        }).start()
    }
}
